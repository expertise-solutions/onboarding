Dia-a-dia
=========

Mantenha seu branch atualizado com o `master`
---------------------------------------------

Para garantir que seu _patch_ não quebre algo que foi versionado no _branch_
`master` depois de ter começado a trabalhar na sua nova tarefa (ou o contrário:
garantir que um commit do `master` não quebre o seu _patch_), é importante
ficar sempre atualizado.

A recomendação então é **todo dia**, _**antes**_ de começar a trabalhar no seu
_patch_:

1. Se tiver alterações não versionadas:

   ```console
   $ git stash
   ```

2. Atualize o `master`:

   ```console
   $ git checkout master
   $ git pull origin master   # supondo que "origin" seja o "git.enlightenment.org/core/efl.git"
   ```

3. Jogue os commits do branch do seu _patch_ em cima dos últimos commits do
   `master`:

   ```console
   $ git checkout -    # "-" volta para o último branch
   $ git rebase master
   ```

4. Se tiver dado `git stash` antes (e ainda queira aquelas alterações de volta):

   ```console
   $ git stash pop
   ```


Mantenha o remoto atualizado
----------------------------

Em resumo, ao final das alterações (ou sempre que quiser garantir que seus
_commits_ estão salvos):

```console
$ git push <remoto-do-gitolite-ou-github> <seu-branch>
```
