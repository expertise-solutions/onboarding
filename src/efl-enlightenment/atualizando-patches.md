Atualizando Patches
===================

Quando você já possui um Diff no Phabricator e precisa atualizá-lo, **depois de
fazer squash dos commits** (dica: você também pode usar `git commit --amend`
quando as alterações não forem muito complexas ao ponto de precisar de vários
commits), use a flag `--update <ID do Diff>`, por exemplo:

```console
$ arc diff HEAD~ --update D12345
```

Nisso, o Arcanist irá pedir para você digitar uma mensagem (como se você
tivesse feito um `git commit` sem `-m`). Essa mensagem é a que será enviada na
_thread_ do Phabricator como um comentário novo e serve para dizer **o que foi
atualizado no patch**.

Se acontecer de precisar atualizar o **sumário** do Diff, você precisa passar a
flag `--edit`:

```console
$ arc diff HEAD~ --update D12345 --edit
```

Situações que você deverá atualizar o sumário:
- Se a descrição do Diff mudou;
- Se **alguma dependência listada já foi adicionada ao `master`**¹;
- Se mudaram os revisores.

> ¹O sistema de resolução de dependências do Arcanist, mesmo sendo automático e
> criado para o uso com o Phabricator, não é das coisas mais espertas, então se
> há um Diff no "Depends On" que já tenha sido aprovado e possui um commit para
> ele (no Phab isso é chamado de _landed_), **ele precisa ser removido da lista
> de "Depends On"** senão o Arcanist, mesmo com o _commit_ já estando no
> `master`, irá tentar reaplicar o Patch e, portanto, gerará conflitos de
> _merge_ (afinal serão duas alterações no mesmo ponto) - que são refletidos na
> criação de arquivos `.rej`, que nada mais são do que partes de _diffs_
> que geram conflitos na hora de aplicar.
