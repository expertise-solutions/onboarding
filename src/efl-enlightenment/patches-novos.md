Trabalhando em um Patch novo
============================

Antes de começar
----------------

Se você assumiu uma tarefa que irá gerar um Patch, crie um _branch_ para sua
alteração a partir do repositório atualizado.

```console
$ git checkout master
$ git pull
$ arc feature my-new-patch
```

A partir dali, faça _commits_ à vontade, como se estivesse trabalhando com um
projeto do GitHub.

Ao final
--------

Tendo prontas as alterações necessárias para disponibilizar o _patch_ para
review:

1. Faça um _**squash**_ dos seus _commits_. Isso pode ser feito dando um
   `rebase -i` a partir do último _commit_ do `master`:

   ```console
   $ git log --oneline --all # Use para procurar o hash do último commit do master
   $ git rebase -i <hash>
   ```
   
   > **squash**
   >
   > Em resumo, é transformar vários _commits_ em apenas um.
   
2. Após o _squash_, crie um Diff a partir do último _commit_:
   
   ```console
   $ arc diff HEAD~    # ou HEAD~1
   ``` 

3. Siga as instruções e, quando o seu editor for aberto para preencher os
   campos necessários para abrir o Diff, siga o seguinte modelo (`<algo>`
   indica que você deve substituir alguma informação, retirando os `<>`):

   ```text
   Summary:
   <descrição de uma ou mais linhas com o que o seu patch faz>

   Reviewers: <nome dos usuários para fazer review (separados por vírgula)>

   Ref T<número da Task do Phabricator associada a este patch>.
   ```

4. Veja o resultado no Phabricator.
