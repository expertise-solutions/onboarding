Comunicação
===========

Slack
-----

O Slack é nosso principal meio de comunicação interna. Ele é separado em canais
para cada tipo de assunto, sendo os principais:

| Canal           | Objetivo                                                       |
| --------------- | -------------------------------------------------------------- |
| **#general**    | Discussões de trabalho (dúvidas, links, etc.).                 |
| **#onboarding** | Para quem estiver começando na empresa.                        |
| **#daily**      | Para postagem das [_dailies_](./dia-a-dia.md#dailies).         |

Não é necessário ser extremamente formal, o importante é ser objetivo da forma
que achar melhor para discutir o que precisa. Apenas lembre de estar **sempre**
conectado ao Slack enquanto estiver no seu horário de trabalho, afinal é bom
que os outros estejam seguros de que podem se comunicar com você por lá.

_(**Dica:** ative as notificações do Slack. Caso julgue interessante, tenha também
o aplicativo do Slack no seu celular - se necessário, configure o horário de
"Não Perturbe", em que o Slack não mandará notificações.)_
