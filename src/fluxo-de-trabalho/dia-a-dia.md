Internos
========

Dailies
-------

A ideia das dailies é simples: todo dia, às 11h, mandar uma mensagem no
[#daily](https://expertisesolutions.slack.com/channel/daily) com:
- O que foi feito no dia anterior;
- O que será feito no dia de hoje.

Infraestrutura
==============

Geral
-----

Na Expertise utilizamos [**Git**](https://git-scm.com) para versionamento.

Servidor
--------

Se tudo estiver certo, nós temos um servidor local para usarmos "à vontade". As
informações estão [na própria página do servidor](./servidor/index.md).
