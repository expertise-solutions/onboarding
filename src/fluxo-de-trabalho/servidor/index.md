Utilização do Servidor
======================

Windows
-------

Nós usamos o [**virt-manager**](https://virt-manager.org/) para gerenciar
(criar, deletar, iniciar, desligar, etc) as máquinas virtuais hospedadas no
servidor e utilizamos [**OpenSSH**](https://www.openssh.com/) para acessar as
máquinas de maneira segura e prática por
[**SSH**](https://en.wikipedia.org/wiki/Secure_Shell).

### Configure **SSH**:

- Instale o **OpenSSH**:

  ```console
  # pacman -S openssh # ArchLinux/Arch-based (Manjaro, EndeavourOS, ...)
  # apt install openssh-client # Debian/Debian-based (Ubuntu, Mint, ...)
  ```

- Gere um par de chaves, caso queira alterar o nome/local do padrão é só
  preencher o primeiro valor, caso contrário pressione `Enter`. Caso queira
  gerar uma chave sem senha (recomendado) pressione `Enter` quando pedir a
  senha:

  ```console
  # ssh-keygen
  ```

- Faça sua chave ser confiável ao servidor (pergunte a senha do servidor à
  alguém):

  ```console
  # ssh-copy-id -i <endereço da chave-publica>.pub root@192.168.15.12
  ```

- Configure o seu ssh para acessar o servidor com o novo par de chaves, edite o
  arquivo `~/.ssh/config` adicionando as seguintes linhas:

  ```console
  AddKeysToAgent yes                              # Confiar nesse endereço e não
                                                  # exigir senha
  Host 192.168.15.12                              # Endereço do servidor
      IdentityFile <endereço da chave-privada>    # A chave-privada a ser usada
  ```

### Configure o **virt-manager**:

- Instale o **virt-manager**:

  ```console
  # pacman -S virt-manager   # ArchLinux/Arch-based (Manjaro, EndeavourOS, ...)
  # apt install virt-manager # Debian/Debian-based (Ubuntu, Mint, ...)
  ```

- Criar a máquina virtual Windows no servidor pelo **virt-manager**_¹_:
    1. Se conecte ao servidor;
    2. Clique com o botão direito e crie uma nova máquina;
    3. Selecione _Local install media_;
    4. Selecione a [**ISO**](https://en.wikipedia.org/wiki/ISO_image) como
       `/var/images/en_windows_10_consumer_editions_version_1909_x64_dvd_be09950e.iso`
       e em chose operational system digite `Windows 10` e seleciona o opção
       `Microsoft Windows 10 (win10)`;
    5. Configure a máquina (RAM 32768 MiB = 32GiB & 20 CPUs)_²_;
    6. Selecione _Enable storage for this virtual machine_ e _Select or create
       custom storage_, e selecione _manage_, lá crie um `Volume` para sua
       máquina com 300Gib_²_(Só o Windows já consome 30Gib), nomeie com algo
       que seja fácil saber que este é o seu volume de windows;
    7. Nomeie seu windows com algo que seja fácil saber que este é o seu.

_¹_ Possivelmente desatualizado se estiver rodando _Windows Server_.

_²_ Verificar esses valores caso ainda esteja usando máquinas individuais e não
    o _Windows Server_.

- Para usar o sistema de acesso remoto do Windows instale o
  [**rdesktop**](https://www.rdesktop.org/):

  ```console
  # pacman -S rdesktop   # ArchLinux/Arch-based (Manjaro, EndeavourOS, ...)
  # apt install rdesktop # Debian/Debian-based (Ubuntu, Mint, ...)
  ```

### Instalação do Windows

- Selecione sua língua/configuração do teclado/fuso horário como quiser;
- Selecione a versão `Windows 10 Pro` (Trivia: A versão `N` é para a versão
  Europeia e a `KN` a Coreana);
- Selecione a versão **off-line**.

#### Habilite o remote desktop do Windows

Para acessar o Windows que está no servidor pelo **rdesktop** é necessário
habilitar o desktop remoto e saber o **ip** dele no servidor:

- Pesquise por _remote desktop settings_ no menu do Windows e habilite o
  _remote desktop_
- Para descobrir o ip do Windows

  ```console
  # ipconfig
  ```

#### Instalações internas do Windows

- Instalar o
  [**Clang-cl**](https://devblogs.microsoft.com/cppblog/clang-llvm-support-in-visual-studio/);
- Instale o [**Visual Studio**](https://visualstudio.microsoft.com/);
- No **Visual Studio** instale os seguintes pacotes:
    - Componentes Individuais
        - C++ Clang Compiler for Windows (9.0.0)
        - C++ Clang-cl for v142 build tools (x64/x86)
    - Workloads
        - .Net desktop development
        - Desktop development with C++
        - Universal Windows plataform development
        - .Net core cross-plataform development

### Uso dia-a-dia

- No **virt-manager** ligue sua máquina virtual (confira se o servidor está
  ligado);
- Acesse sua máquina virtual com o **rdesktop**:

  ```console
  # rdesktop <ip do seu windows no servidor>
  ```

Por facilidade de desenvolvimento você pode também compartilhar uma pasta do
seu Linux (possivelmente a home do seu usuário) com o rdesktop para acessar no
Windows:

```console
# rdesktop <ip do seu windows no servidor> -r disk:mydisk=<endereço>
```
