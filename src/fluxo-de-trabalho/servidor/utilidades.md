Utilidades
==========

Acessar uma pasta do Windows a partir do Linux
----------------------------------------------

### 1. No Windows

1. Botão direito na pasta que será compartilhada -> **"Propriedades"**;
2. Vá na aba **Sharing** e clique em **"Advanced Sharing"**;
3. Marque a opção "**Share this folder** e dê um OK;
4. Ainda nas propriedades da pasta, vá na aba **Security**;
5. Clique em **Edit** -> **Add**;
6. Na caixa de texto, escreva "Everyone" e dê um OK;
7. Selecione o recém criado "Everyone" na lista "Group or user names";
8. Em "Permissions for Everyone", clique na checkbox do "Allow" para a opção
   "Full control";
9. Dê Apply/OK/Close toda vida até voltar à aba **Advanced Sharing** das propriedades;
10. Clique em **Permissions**;
11. Selecione o recém criado "Everyone" na lista "Group or user names";
12. Em "Permissions for Everyone", clique na checkbox do "Allow" para a opção
   "Full control";
13. Dê Apply/OK/Close toda vida até voltar à aba **Sharing** das propriedades;
14. Copie para algum lugar o "Network Path" (e dê um OK para fechar a janela).

### 2. No Linux

1. Certifique-se de que você possui o comando `mount.cifs`;
2. Substituindo `<O Network Path>` (**lembrando de substituir todo `\` por
   `/`**) e `SEU_USUARIO_NO_WINDOWS`, execute:

   ```console
   $ mkdir windows      # Ou qualquer pasta que queira montar eu windows
   # sudo mount.cifs <O Network Path> windows -o uid=$(id -u),gid=$(id -g),user=SEU_USUARIO_NO_WINDOWS
   ```

   Por exemplo, se o Network Path for `"//WIN-G1BB3R15H/source"` e o usuário for `"Lucas"`:

   ```console
   $ mkdir windows
   # sudo mount.cifs "//WIN-G1BB3R15H/source" windows -o uid=$(id -u),gid=$(id -g),user=Lucas
   ```

   **OBS**: O `uid=$(id -u)` e `gid=$(id -g)` é para montar para o seu usuário
   atual do Linux em vez de `root`.

3. Pronto, agora você deve ter sua pasta `windows` contendo sua pasta
   compartilhada como esperado.

#### Montando o repositório remoto autômaticamente
1. Gere a tabela de partições:
    ```console
        genfstab /
    ```
2. Adicione as linhas relacionadas ao Windows ao `/etc/fstab`


