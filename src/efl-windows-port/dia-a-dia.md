Uso dia-a-dia
=============

Conexão com o Servidor
----------------------

As maneiras atuais são:
- Por uma instância visual do Windows pelo
  [rdesktop](/src/fluxo-de-trabalho/servidor/index.md#uso-dia-a-dia);
- Utilizando o ssh para logar no seu usuário (por onde você pode rodar os
  scripts de configuração/compilação/etc) e realizando as alterações no projeto
  pelo [Linux através de uma pasta compartilhada com o
  Windows](/src/fluxo-de-trabalho/servidor/utilidades.md);

Resolvendo uma Dependência
--------------------------
O branch principal do nosso port é o
`devs/jptiz/build-on-windows-with-clang-cl` no [github da
expertise](https://github.com/expertisesolutions/efl). Partindo dele, é necessário
estudar o que a EFL usa dessa dependência, se o Windows implementa algo similar
nativamente, se já existem alternativas implementadas e com isso decidir como
remover/resolver essa dependência. Não existe uma fórmula, é realmente um caso a
caso a partir daqui.

Quanto terminar é só confirmar que todas as suas alterações estão commitadas e
```console
  git push <remoto da expertise> devs/<seu nick>/<a tarefa que está desenvolvendo>
```

Mantendo o seu repositório atualizado
-------------------------------------
É importante manter seu repositorio atualizado com relação a [EFL](https://git.enlightenment.org/core/efl.git) e com relação ao nosso "`master`" (devs/jptiz/build-on-windows-with-clang-cl):
```console
  git checkout master
  git pull <remoto da efl> master
  git checkout devs/jptiz/build-on-windows-with-clang-cl
  git pull <remoto da expertise> devs/jptiz/build-on-windows-with-clang-cl
  git rebase master
  git checkout devs/<seu nick>/<a tarefa que está desenvolvendo>
  git rebase devs/jptiz/build-on-windows-with-clang-cl
```
Depois de atualizar a sua branch com relação a nossa `master` pode ser
necessário forçar sua arvore local sobre a remota (só faça isso se tiver certeza
que não está apagando nada e evite fazer isso em branchs compartilhadas,
qualquer coisa peça ajuda ;D):
```console
  git push <remoto da expertise> devs/<seu nick>/<a tarefa que está desenvolvendo> --force
```
