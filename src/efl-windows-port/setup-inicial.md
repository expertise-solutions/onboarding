Instalações internas do Windows_¹_
==================================

- Instalar o
  [**Clang-cl**](https://devblogs.microsoft.com/cppblog/clang-llvm-support-in-visual-studio/)
- Instale o [**Visual Studio**](https://visualstudio.microsoft.com/).
- No **Visual Studio** instale os seguintes pacotes:
    - Componentes Individuais:
        - C++ Clang Compiler for Windows (9.0.0);
        - C++ Clang-cl for v142 build tools (x64/x86).
    - Workloads:
        - .Net desktop development;
        - Desktop development with C++;
        - Universal Windows plataform development;
        - .Net core cross-plataform development.

_¹_ Estamos utilizando um Windows Server então algumas coisas já estão
    instaladas globalmente

Dependencias de build da EFL
============================

Agora é necessário compilar o
[**OpenSSL**](https://github.com/openssl/openssl#download) e instalar o
[**PCRE**].

OpenSSL
-------

No geral, é necessário clonar o [código
fonte](https://github.com/openssl/openssl) e seguir as instruções em
[`INSTALL.md`](https://github.com/openssl/openssl/blob/master/INSTALL.md) e em
[`NOTES.WIN`](https://github.com/openssl/openssl/blob/master/NOTES.WIN).

### Dependências do OpenSSL

#### [NASM](https://www.nasm.us/)

Disponível por meio de um `.exe` no [site](https://www.nasm.us/) e adicione ele ao Path:
    1. Menu do Windows;
    2. Pesquise por `Edit environment variables for your account`;
    3. Selecione `Path` e edite;
    4. Adicione o caminho para a pasta onde o `NASM` foi instalado.

#### Perl

O OpenSSL recomenda o [ActiveState Perl](https://www.activestate.com/), mas por
simplicidade estamos usando o [Strawberry Perl](http://strawberryperl.com/)_²_.

_²_ O Strawberry Perl já está instalado **system-wide** no servidor como
    `perl5.30.1.exe`.

### Compilando o OpenSSL
1. Abra a pasta do **OpenSSL** no `Visual Studio`;
2. Clique com o botão direito na pasta do OpenSSL;
3. Abra o `Developer Command Prompt`;
4. Siga instruções de instalação no Windows em
   [`INSTALL.md`](https://github.com/openssl/openssl/blob/master/INSTALL.md).

_PS¹_: Lembre de usar o [**Perl** correto](#Perl)
_PS²_: Altere o `--prefix` e o `--openssldir` para você utilizar sua própria
    versão da **OpenSSL** posteriormente

#### Problemas

1. Caso alguma biblioteca x86 esteja sendo, usada rode o seguinte comando para
   utilizar as o compilador x64 e em seguida tente os passos de compilação novamente:

   ```console
   "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvars64.bat"
   ```
2. Ao clonar a **OpenSSL** o `Visual Studio` realiza um make, verifique se
   houve algum erro nele e resolva-os;
3. Verifique se está utilizando o [`Perl` correto](#Perl):

   ```console
   perl5.30.1.exe Configure VC-WIN64A --prefix=C:/Users/Lucas/AppData/Local/bin/OpenSSL --openssldir=C:/User/Lucas/AppData/Local/bin/openssldir
   ```

PCRE
----

Baixe o zip developer files do [**PCRE**](http://gnuwin32.sourceforge.net/packages/pcre.htm)

LLVM
----

Baixe o exe da última versão estável do
[**LLVM**](https://github.com/llvm/llvm-project/releases/)

_PS_: A versão 9.0.1 do **LLVM** já está instalada **system-wide**.

Preparar o repositorio
======================

- Agora é necessario clonar
  [**EFL**](https://git.enlightenment.org/core/efl.git). Caso se sinta
  confortável com o `Visual Studio`, faça isso por ele (a opção para clonar um
  repositorio git aperece ao abrir o `Visual Studio`), caso contrario é
  possivel usar outros editores de texto em conjunto com o
  `CMD`/`PowerShell`/`git-bash` (uma interface com comandos básicos `UNIX` e
  para uso do **Git**).

- Adicione o `remote` da [Expertise Solutions](https://github.com/expertisesolutions/efl.git):
  ```console
    git remote add <nome pro remote da expertise> https://github.com/expertisesolutions/efl.git  # HTTPS
    git remote add <nome pro remote da expertise> git@github.com:expertisesolutions/efl  # SSH
  ```

- O ideial é partir da branch
  [devs/jptiz/build-on-windows-with-clang-cl](https://github.com/expertisesolutions/efl/tree/devs/jptiz/build-on-windows-with-clang-cl)
  que já possui algumas configurações iniciais do Felipe e do @jptiz, ela está
  sevindo como uma `master` no nosso projeto.  Dê um fetch nela e em seguida
  faça sua propria branch:


  ```console
    git fetch <nome pro remote da expertise> devs/jptiz/build-on-windows-with-clang-cl
    git checkout devs/jptiz/build-on-windows-with-clang-cl
    git checkout -b <sua branch de trabalho>
  ```
