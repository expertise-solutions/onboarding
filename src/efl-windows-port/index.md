No momento a [EFL](https://www.enlightenment.org/about-efl) já possui um versão
que funciona em [Windows](https://phab.enlightenment.org/w/windows/), porém ela
usa [MYSYS2](https://www.msys2.org/)/[CYGWIN](https://www.cygwin.com/), que
distribui ferramentas da GNU (além de outras) Open Source para o Windows.
Ou seja, o porte atual não é nativo, ele utiliza de várias bibliotecas que
permitem a utilização da implementação original da EFL no Windows.

Esse projeto tem como intuito portar a EFL de maneira **nativa** para o Windows.

