Compilando a EFL
================

Lembre-se que para começar a compilar é necessário o meson como descrito na
[seção ferramentas](#/src/efl-enlightenment/ferramentas.md/Ferramentas) e
para usá-lo no Windows é preciso adicionar ele ao `Path` (`<User>/AppData/Roaming/Python/Scripts`).

O script `configure.bat` configura o Meson para _buildar_ a EFL com o
`clang-cl` passando os diretórios das dependências. Caso eles não estejam
configurados, ele pede para que você os insira em `env.bat`. Esse script tem
uma função similar de rodar `meson setup` com o adicional das alterações feitas
para o port.

O script `build.bat` faz a compilação, da mesma forma que o `ninja` fazia isso,
com o adicional de configurar as variáveis de ambientes necessárias primeiro.

**TODO**

- pthread
- unistd.h - talvez desabilitar
- sys/time.h
- api de arquivos posix - evol - biblioteca de compatibilidade windows


