Preparações
===========

Primeiramente, seja bem-vindo à ExpertiseSolutions!

Como novo integrante, esta seção será seu ponto de partida para entender como
as coisas funcionam por aqui. Talvez não seja necessário ler detalhadamente
cada uma das páginas, mas ao menos fica como forma de consultar caso tenha
dúvidas.

Se não encontrar algo por este documento todo, **pergunte**. ;)

Contas
------

A primeira coisa que é necessário se certificar é que você tenha acesso a:

- [ ] **G-Suite**:
  - Garanta que tenha sido adicionado à equipe e tenha um e-mail
    `@expertisesolutions.com.br`. Se não tiver, fale com alguém da equipe;
- [ ] **Slack**:
  - Entre em <https://expertisesolutions.slack.com> com seu e-mail
    `@expertisesolutions.com.br`;
